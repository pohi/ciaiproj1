/**
 * 
 */

var Home = React.createClass({
    render: function() {
        return (
        	<div>
           <NavSignOut/>
           <Jumbotron/>
           <Table/>
           </div>
           
            )
    }
});


var HomeSign = React.createClass({
    render: function() {
        return (
        	<div>
           <NavSignIn/>
           <Jumbotron/>
           <Table/>
           </div>
           
            )
    }
});

var HomeSignProf = React.createClass({
    render: function() {
        return (
        	<div>
           <NavSignInProf/>
           <Jumbotron/>
           <Table/>
           </div>
           
            )
    }
});




var NavSignIn = React.createClass({
    render: function() {
        return (
        		<nav className="navbar navbar-inverse">
    			  <div className="container-fluid">			   
    			    <div className="navbar-header">
    			      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    			        <span className="sr-only">Toggle navigation</span>
    			        <span className="icon-bar"></span>
    			        <span className="icon-bar"></span>
    			        <span className="icon-bar"></span>
    			      </button>
    			      <Link to={'/sign'} className="navbar-brand">CLIP</Link>
    			    </div>			  
    			    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">			      			        
    			    <ul className="nav navbar-nav">	
    				<li><Link to={'/profil'}>Profil</Link></li>
    				<li><Link to={'/dashboard'}>Dashboard</Link></li>
                  </ul>			      
    			      <form className="navbar-form navbar-right" >	        
    			        <button type="submit" className="btn btn-danger navbar-btn"><Link to={'/'}>Logout</Link></button>
    			        </form>		      
    			    </div>			   
    			  </div>
    			</nav>          
            )
    }
});


var NavSignInProf = React.createClass({
    render: function() {
        return (
        		<nav className="navbar navbar-inverse">
    			  <div className="container-fluid">			   
    			    <div className="navbar-header">
    			      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    			        <span className="sr-only">Toggle navigation</span>
    			        <span className="icon-bar"></span>
    			        <span className="icon-bar"></span>
    			        <span className="icon-bar"></span>
    			      </button>
    			      <Link to={'/sign'} className="navbar-brand">CLIP</Link>
    			    </div>			  
    			    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">			      			        
    			    <ul className="nav navbar-nav">	
    				<li><Link to={'/professor'}>ProfilProf</Link></li>
    				<li><Link to={'/dashboard'}>Dashboard</Link></li>
                  </ul>			      
    			      <form className="navbar-form navbar-right" >	        
    			        <button type="submit" className="btn btn-danger navbar-btn"><Link to={'/sign'}>Logout</Link></button>
    			        </form>		      
    			    </div>			   
    			  </div>
    			</nav>          
            )
    }
});




var NavSignOut = React.createClass({
    render: function() {
        return (
        		<nav className="navbar navbar-inverse">
  			  <div className="container-fluid">
  			   
  			    <div className="navbar-header">
  			      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
  			        <span className="sr-only">Toggle navigation</span>
  			        <span className="icon-bar"></span>
  			        <span className="icon-bar"></span>
  			        <span className="icon-bar"></span>
  			      </button>
  			      <a className="navbar-brand" href="#">CLIP</a>
  			    </div>			  
  			    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">  
  			      <form className="navbar-form navbar-right" >
  			        <div className="form-group">
  			          <input type="text" className="form-control" placeholder="Username"/>
  			        </div>
  			        <button type="submit" className="btn btn-danger navbar-btn"><Link to={'/sign'}>Login</Link></button>
  			       <button type="submit" className="btn btn-danger navbar-btn"><Link to={'/signProf'}>LoginProfessor</Link></button>
  			        </form>  
  			    </div>			   
  			  </div>
  			</nav>         
            )
    }
});


var Jumbotron = React.createClass({	
	render: function(){    	
    	return (    			 
    		<div>
    		<div className="jumbotron" style={{paddingLeft:50}}>    			
  			  <h2><strong>Welcome in a CLIP!</strong></h2>
  			  <p>If you are not registered yet, don't hesitate, sing up and start use CLIP.</p>
  			  <p><a className="btn btn-danger" href="#" role="button">Sign up</a></p>    			
  			 </div>  			
  			 
  		  </div>
            
        );
    }
});


var Table = React.createClass({	
	render: function(){
                return (  
                	    <div style={{marginLeft: 50,marginRight:50}}>	
                		<h4><strong>Here you can see list of available degrees:</strong></h4>
                		<br/>
                		<div className="panel panel-success" >
                		  <div className="panel-heading">
                		    <h3 className="panel-title">
                		    <div className="row">
	                		  <div className="col-md-2 col-xs-4" >Name</div>
	                		  <div className="col-md-4 col-xs-6">Description</div>
	                		  <div className="col-md-1 col-xs-2">ECTS</div>
	                		  <div className="col-md-1 col-xs-2">Years</div>
	                		  <div className="col-md-4 col-xs-6">Courses</div>                		 
	                		</div>         		    
                		    </h3>
                		  </div>
                		  <div className="panel-body">
	                		  <div className="row">
	                		  <div className="col-md-2 col-xs-4">Computer science</div>
	                		  <div className="col-md-4 col-xs-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
	                		  <div className="col-md-1 col-xs-2">120</div>
	                		  <div className="col-md-1 col-xs-2">4</div>
	                		  <div className="col-md-4 col-xs-6">Lorem, Ipsum, simply, dummy, text</div>              		 
	                		</div>
	                		<br/>  
	                		
	                		<div className="row">
	                		 <div className="col-md-2 col-xs-4">Mathematics</div>
	                		  <div className="col-md-4 col-xs-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
	                		  <div className="col-md-1 col-xs-2">114</div>
	                		  <div className="col-md-1 col-xs-2">3</div>
	                		  <div className="col-md-4 col-xs-6">Lorem, Ipsum, simply, dummy, text</div>             		 
	                		</div>
	                		<br/> 
	                		
	                		<div className="row">
	                		 <div className="col-md-2 col-xs-4">Chemistry</div>
	                		  <div className="col-md-4 col-xs-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
	                		  <div className="col-md-1 col-xs-2">78</div>
	                		  <div className="col-md-1 col-xs-2">5</div>
	                		  <div className="col-md-4 col-xs-6">Lorem, Ipsum, simply, dummy, text</div>             		 
	                		</div>
	                		<br/>       		  
                		  </div>
                		  <div className="panel-footer panel-success"></div>
                		</div>   
                		</div> 
                );
            }
        });





var StudentName = React.createClass({
    render: function() {
        return (
            <strong>Joao Couto</strong>
            )
    }
});
 
var StudentInfo = React.createClass({
	
	getInitialState: function() {      
        return {items: ['0944 018 478','write@me.pls','Rua de Rui Cunhada 42, Lisbon'], text: '', text2: '', text3: '', edit: false};
    },
    activeEdit: function(e) {
          e.preventDefault();
            this.setState({ edit: true });     
    },
    deactiveEdit: function(e) {
            e.preventDefault();
            if(this.state.text != '') this.state.items[0] = this.state.text;
		  	if(this.state.text2 != '') this.state.items[1] = this.state.text2;
		  	if(this.state.text3 != '') this.state.items[2] = this.state.text3;
            this.setState({ edit: false });    
    },
    changeTel: function(e) {
            this.setState({text: e.target.value});
    },
    changeMail: function(e) {
            this.setState({text2: e.target.value});
    },
    changeAdr: function(e) {
            this.setState({text3: e.target.value});
    },
   
    render: function() {
        var show;	       
	   
            if (!this.state.edit){
                show =  <div className="student-info">
                    <h2><StudentName /></h2>
                        <figure style={{marginRight: 1 + 'em'}}>
                            <img src="/assets/img/default.png" alt="Default avavatar" width="100" height="100" />
                            <figcaption><StudentName /></figcaption>
                            <span>{this.state.items[0]}</span><br/>
                            <span>{this.state.items[1]}</span><br/>
                            <span>{this.state.items[2]}</span>
               
                         </figure>
                         <button type="button" className="btn btn-primary" onClick={this.activeEdit}>Edit</button>
                    </div>
            }
            else{
                 show = <div className="student-info">
                    <h2><StudentName /></h2>
                        <figure style={{marginRight: 1 + 'em'}}>
                            <img src="/assets/img/default.png" alt="Default avavatar" width="100" height="100" />
                            <figcaption><StudentName /></figcaption>
                            <span><input type="text" name="tel" defaultValue={this.state.items[0]} onChange={this.changeTel} /></span><br/>
                            <span><input type="text" defaultValue={this.state.items[1]} onChange={this.changeMail} /></span><br/>
                            <span><input type="text" defaultValue={this.state.items[2]} onChange={this.changeAdr} /></span>
               
                         </figure>
                         <button type="button" className="btn btn-primary" onClick={this.deactiveEdit}>Save</button>
                    </div>
                   
            }
           
          return(                
                    <div>            
                    {show}      
                   </div>              
            );
    }
           
}); 
 
 
var StudentDetails = React.createClass({
    render: function () {
        return (
        		 <div>
     	    	<NavSignIn/>
                <div className="container-fluid">
 
                <div className="row">
                    <div className="col-md-12">
                        <h1>My profile</h1>
                    </div>
                </div>
 
                <div className="row">
                    <div className="col-md-4">
                        <StudentInfo />
                    </div>
 
                    <div className="col-md-4">
                        <div className="finished-courses">
                            <h2>Finished courses:</h2>
                            <div className="row">
                                    <div className="col-md-6"> 
                                        <strong> Course name </strong><br/>
                                         <a href="course.html">Mathematics</a><br/>
                                         <a href="course.html">CIAI</a><br/>
                                         <a href="course.html">IPM</a><br/>
                                         <a href="course.html">CGI</a>                                 
                                    </div>                                 
                                    <div className="col-md-6">
                                    <strong>Final grade</strong><br/>
                                        C <br/>
                                        D <br/>
                                        A <br/>
                                        A                          
                                    </div>
                            </div>                           
                        </div>
                    </div>
 
                    <div className="col-md-4">
                        <div className="enrolled-courses">
                            <h2>Enrolled courses:</h2>
                            <ul>
                                <li><a href="course.html">Data analysis</a></li>
                                <li><a href="course.html">Cloud computing</a></li>
                                <li><a href="course.html">Machine larning</a></li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>
            </div>           
            )
    }
});


var Dashboard = React.createClass({
	
	render: function () {
        return (
                <div>
                <NavSignIn/>  			
  			<div className="panel panel-danger" style={{marginLeft: 50,marginRight:50}} >
  		  <div className="panel-heading">
  		    <h3 className="panel-title">
  		    <div className="row">
      		  <div className="col-md-12 col-xs-4" >Computer science</div>      		                 		 
      		</div>         		    
  		    </h3>
  		  </div>
  		  <div className="panel-body">
      		  <div className="row">
	      		  <div className="col-md-1 col-xs-2"><strong>Course</strong></div>
		   		  <div className="col-md-4 col-xs-6"><strong>Description</strong></div>
		   		  <div className="col-md-1 col-xs-2"><strong>ECTS</strong></div>
		   		  <div className="col-md-2 col-xs-4"><strong>Teachers</strong></div>
		   		  <div className="col-md-4 col-xs-6"><strong>Enrolled students</strong></div>              		 
      		</div>
      		<br/>        		
      		<div className="row">
      		 <div className="col-md-1 col-xs-2">Computer graphics</div>
      		  <div className="col-md-4 col-xs-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
      		  <div className="col-md-1 col-xs-2">6</div>
      		  <div className="col-md-2 col-xs-4">Lorem Ipsum, Lorem Ipsum</div>
      		  <div className="col-md-2 col-xs-4">Anna Klacikova, Peter Maly, Dominik Knechta</div>
      		 <div className="col-md-2 col-xs-4"><Link to={'/dashboard/computergraphics'}>see marks</Link></div>   
      		</div>
      		<br/>       		
      		<div className="row">
      		 <div className="col-md-1 col-xs-2">Web search</div>
     		  <div className="col-md-4 col-xs-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
     		  <div className="col-md-1 col-xs-2">4</div>
     		  <div className="col-md-2 col-xs-4">Lorem Ipsum, Lorem Ipsum</div>
     		  <div className="col-md-2 col-xs-4">Jana Klacikova, Peter Maly, Dominik Knechta, Sara Horovska, Veronika Pohorencova</div>  
     		 <div className="col-md-2 col-xs-4">see marks</div> 
     		</div>
      		<br/>       		  
  		  </div>
  		  <div className="panel-footer panel-danger"></div>
  		</div>   
  		
  		
  		
  		<div className="panel panel-danger" style={{marginLeft: 50,marginRight:50}} >
		  <div className="panel-heading">
		    <h3 className="panel-title">
		    <div className="row">
    		  <div className="col-md-12 col-xs-4" >Mathematics</div>      		                 		 
    		</div>         		    
		    </h3>
		  </div>
		  <div className="panel-body">
    		  <div className="row">
	      		  <div className="col-md-1 col-xs-4"><strong>Course</strong></div>
		   		  <div className="col-md-4 col-xs-6"><strong>Description</strong></div>
		   		  <div className="col-md-1 col-xs-2"><strong>ECTS</strong></div>
		   		  <div className="col-md-2 col-xs-2"><strong>Teachers</strong></div>
		   		  <div className="col-md-4 col-xs-6"><strong>Enrolled students</strong></div>              		 
    		</div>
    		<br/>  
    		
    		<div className="row">
    		 <div className="col-md-1 col-xs-4">Algebra</div>
    		  <div className="col-md-4 col-xs-6">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
    		  <div className="col-md-1 col-xs-2">3</div>
    		  <div className="col-md-2 col-xs-2">Lorem Ipsum, Lorem Ipsum</div>
    		  <div className="col-md-4 col-xs-6">Anna Klacikova, Peter Maly, Dominik Knechta</div>             		 
    		</div>
    		<br/>     		
    		       		  
		  </div>
		  <div className="panel-footer panel-danger"></div>
		</div>           		
            </div>
            )
    }
});

var Marks = React.createClass({
    render: function() {
        return (   
            <div>		
            <NavSignIn/>
            <div style={{marginLeft: 50,marginRight:50}}>	
    		
    		<div className="panel panel-success" >
    		  <div className="panel-heading">
    		    <h3 className="panel-title">
    		    <div className="row">
        		  <div className="col-md-12 col-xs-4" >Computer graphics</div>        		               		 
        		</div>         		    
    		    </h3>
    		  </div>
    		  <div className="panel-body">
        		  <div className="row">
        		  <div className="col-md-4 col-xs-6"><strong>Student</strong></div>
		   		  <div className="col-md-2 col-xs-4"><strong>Test1</strong></div>
		   		  <div className="col-md-2 col-xs-4"><strong>Test2</strong></div>
		   		  <div className="col-md-2 col-xs-4"><strong>Homeworks</strong></div>
		   		  <div className="col-md-2 col-xs-4"><strong>Mark</strong></div>             		 
        		</div>
        		<br/>  
        		
        		<div className="row">
        		  <div className="col-md-4 col-xs-6">Anna Klacikova</div>
		   		  <div className="col-md-2 col-xs-4">25</div>
		   		  <div className="col-md-2 col-xs-4">14</div>
		   		  <div className="col-md-2 col-xs-4">50</div>
		   		  <div className="col-md-2 col-xs-4">C</div>             		 
        		</div>
        		<br/> 
        		
        		<div className="row">
        		  <div className="col-md-4 col-xs-6">Peter Maly</div>
		   		  <div className="col-md-2 col-xs-4">10</div>
		   		  <div className="col-md-2 col-xs-4">7</div>
		   		  <div className="col-md-2 col-xs-4">25</div>
		   		  <div className="col-md-2 col-xs-4">E</div>            		 
        		</div>
        		<br/>  
        		<div className="row">
      		   <div className="col-md-4 col-xs-6">Dominik Knechta</div>
		   		  <div className="col-md-2 col-xs-4">30</div>
		   		  <div className="col-md-2 col-xs-4">30</div>
		   		  <div className="col-md-2 col-xs-4">50</div>
		   		  <div className="col-md-2 col-xs-4">A</div>            		 
	      		</div>
	      		<br/> 
    		  </div>
    		  <div className="panel-footer panel-success"></div>
    		</div>   
    		</div> 
            </div>            
            
            )
    }
});


var ProfName = React.createClass({
    render: function() {
        return (
            <strong>Doctor Strange</strong>
            )
    }
});
 
var ProfActions = React.createClass({
    render: function() {
        return (
            <div>
                <Link to={'/dashboard'} className="btn btn-success">Students adminstration (enrolling)</Link>
                <br />
                <Link to={'/dashboard/computergraphics'} className="btn btn-success">Students grades</Link>
            </div>
            )
    }
});
 
var ProfInfo = React.createClass({
    getInitialState: function() {      
        return {items: ['write@me.pls','MEI'], text: '', text2: '', text3: '', edit: false};
    },
    activeEdit: function(e) {
          e.preventDefault();
            this.setState({ edit: true });    
    },
    deactiveEdit: function(e) {
            e.preventDefault();      
            if(this.state.text2 != '') this.state.items[0] = this.state.text2;
            if(this.state.text3 != '') this.state.items[1] = this.state.text3;
            this.setState({ edit: false });    
    },
    changeMail: function(e) {
            this.setState({text2: e.target.value});
    },
    changeAdr: function(e) {
            this.setState({text3: e.target.value});
    },
   
    render: function() {
        var show;
   
   
            if (!this.state.edit){
                show =  <div className="student-info">
                    <h2><ProfName /></h2>
                        <figure style={{marginRight: 1 + 'em'}}>
                            <img src="/assets/img/default2.png" alt="Default avavatar" width="100" height="100" />
                            <figcaption><ProfName /></figcaption>
                            <span>{this.state.items[0]}</span><br/>
                            <span>Department: {this.state.items[1]}</span>
               
                         </figure>
                         <button type="button" className="btn btn-primary" onClick={this.activeEdit}>Edit</button>
                    </div>
            }
            else{
                 show = <div className="student-info">
                    <h2><ProfName /></h2>
                        <figure style={{marginRight: 1 + 'em'}}>
                            <img src="/assets/img/default2.png" alt="Default avavatar" width="100" height="100" />
                            <figcaption><ProfName /></figcaption>
                            <span><input type="text" name="tel" defaultValue={this.state.items[0]} onChange={this.changeMail} /></span><br/>
                            <span>Department:<input type="text" defaultValue={this.state.items[1]} onChange={this.changeAdr} /></span>
               
                         </figure>
                         <button type="button" className="btn btn-primary" onClick={this.deactiveEdit}>Save</button>
                    </div>
            }
          return(                
                    <div>            
                    {show}      
                   </div>              
            );
    }
           
});
 
 
 
 
 
var ProfDetails = React.createClass({
    render: function () {
        return (
        		<div>
        		<NavSignInProf/>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <h1>My teaching</h1>
                        </div>
                    </div>
     
                    <div className="row">
                        <div className="col-md-4">
                            <ProfInfo />
                            <br/>
                            <ProfActions />
                        </div>
                    </div>
                </div>
                </div>
           
            )
    }
});











 



var Link = ReactRouter.Link;
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;


ReactDOM.render((
		<Router>
	    	<Route path="/" component={Home}/>	    	
	    	<Route path="/sign" component={HomeSign}/>
	    	<Route path="/signProf" component={HomeSignProf}/>
	    	<Route path="/profil" component={StudentDetails}/>
	    	<Route path="/dashboard" component={Dashboard}/>
	    	<Route path="/dashboard/computergraphics" component={Marks}/>
	    	<Route path="/professor" component={ProfDetails}/>
	    	</Router>
		), document.getElementById('main'))